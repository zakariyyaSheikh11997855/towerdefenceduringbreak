﻿using UnityEngine;
using UnityEngine.Networking;

public class Base : BaseHealth {

	void Start () {
		currentHealth = maxHealth;
	}

	[ClientRpc]
	public override void RpcTakeDamage(float _amount){
		currentHealth -= _amount;

		if(currentHealth <= 0)
		{
			Die();
		}
	}

	protected override void Die(){
		//here goes explode stuff + stuff which should happen after explosion


		base.Die ();
	}
}
